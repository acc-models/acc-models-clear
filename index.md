---
template: overrides/main.html
---

# CLEAR Beamline Optics Repository

This repository contains the MAD-X model of the CLEAR beamline, and it is rendered also on the official [CERN Optics Respository](http://acc-models.web.cern.ch/acc-models/clear/)
The official CLEAR website is [clear.cern](http://clear.cern)

##  Optics

CLEAR is a multi-purpose test facility, so there is no official _stable_ optics. Instead, the optics is continuously adapted to fulfill the user needs.

Some examples optics, included in different `scenarios` folder in the [GIT repository](https://gitlab.cern.ch/acc-models/acc-models-clear), are:

#### VESPER experimental area

TFS table of the optics available [here](scenarios/vesper/vesper.tfs){target=_blank}.
<object width="100%" data="scenarios/vesper/vesper.html"></object> 

#### CLIC experimental program

TFS table of the optics available [here](scenarios/clic/clic.tfs){target=_blank}.
<object width="100%" data="scenarios/clic/clic.html"></object> 

#### Plasma Lens experiment

TFS table of the optics available [here](scenarios/plasmalens/plasmalens.tfs){target=_blank}.
<object width="100%" data="scenarios/plasmalens/plasmalens.html"></object> 

#### InAir or THz experimental area

TFS table of the optics available [here](scenarios/thz/thz.tfs){target=_blank}.
<object width="100%" data="scenarios/thz/thz.html"></object> 
