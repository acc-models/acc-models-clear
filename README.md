# CLEAR Optics Repository

Contributors since 2016:

 - Wilfrid Farabolini
 - Sumaira Zeeshan
 - Davide Gamba
 - Kyrre Ness Sjobak


This repository contains the MAD-X model of the CLEAR beamline.
The official CLEAR website is [clear.cern](http://clear.cern)

## Repository Description

The MAD-X element definitions and sequences are contained in the single `clear.seqx` file.
There are three start-to-end sequences with typical output file names, which represent a possible start-to-end view of the accelerator:

- `VESPER' / "CALIFES"  : Injector linac, VESPER dump
- `LINE'   / "CLEAR"    : Injector linac, experimental line, spectrometer dump
- `STLINE' / "clearST"  : Injector linac, experimental line, straight dump

The "type" of beams that are typically generated at the end of the CALIFES linac are stored in the `beams` folder.

The typical machine configurations, i.e. quadrupoles strengths and relative optics and scripts to generate them, are stored in the `scenarios` folder.

Additional folders:

- `survey` folder contains MAD-X script to generated the survey files, as well as tables of element positions in TFS format. Note that:
    - the element s-position in the survey tables indicate the magnetic end of the element
    - the element lengths are the magnetic lengths
    - Output survey tables are produced in
		- The global CERN coordinate system (`survey`)
		- A local coordinate system starting at the gun (`survey0`)
		- A local coordinate system with only the most relevant elements included (`survey0_filtered`).
    - Surveys are generated for the three start-to-end configurations described above.
- `operation` folder contains the JMAD XML configuration 
- `madxTesting` contains an example (and its output) for MAD-X testing purposes. **It is not meant to be changed/used!**
- `_scripts` folder contains a few python scripts used to build the ACC-Models-CLEAR website and the JMAD XML configuration 
- `.gitlab-ci.yml` file contains the scripts which are run by gitlab when you push a new version of the model. Typically this is needed to re-populate the acc-models website.

## Generic GIT Commands:

The repository is managed with git, enabling version-control in an easy-to access central repository hosted at CERN gitlab: https://gitlab.cern.ch/acc-models/acc-models-clear .
A summary of some basic git commands are given below.
If you choose, you can also use a graphical client or directly download files from the repository web-page.

- download the project 
```
git clone https://gitlab.cern.ch/acc-models/acc-models-clear.git
```

- update the local repository with the remote repository
```
git pull
```

- See changes in your local copy relative to what is stored in the repository:
```
git diff
```
Note that if you have commited anything, even locally without uploading with `push`, git will show you the changes relative to the latest comitted version.

To update the REMOTE repository with changes, please use branhces and merge requests.
For more information, contact Kyrre Sjobak ( kyrre DOT ness DOT sjoebaek PIGTAIL cern DOT ch )