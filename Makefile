# Simple MakeFile to build different parts of this repository
#
# For some python scripts, make sure you installed the packages in `_scripts/packages.txt`
#  or use the provided `make pypackage` target
# 
# Davide - Feb 2023

# To set the command to use for MADX, run like
# make MADX=madx
# if e.g. the command to run madx on your system is 'madx'.

# MAD-X executable:
ifndef MADX
  MADX = ~/CERN/Bin/MADX/madx-macosx64-gnu
  $(echo "Assuming madx executable is in MADX")
endif

all : optics survey bokeh xml
 
optics : clear.seqx clear.dbx scenarios/*/*.madx scenarios/*/*.str beams/*.beam 
	for i in $$(ls scenarios); do \
	   ls scenarios/$$i ; \
	   cd scenarios/$$i ; \
	   $(MADX) < $$i.madx ; \
	   cd ../../ ; \
	done \

survey : clear.seqx survey/runSurvey.madx
	cd survey; $(MADX) < runSurvey.madx

madxtest : clear.seqx clear.dbx madxTesting/test.madx
	cd madxTesting ; $(MADX) < test.madx ; cd .. ;

xml :
	python _scripts/create_XML.py

bokeh :
	python _scripts/create_bokeh.py

pypackage :
	for package in `cat _scripts/packages.txt`; do pip install $$package; done
